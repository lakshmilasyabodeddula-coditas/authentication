import { Injectable } from '@angular/core';
import { HttpHandler,HttpRequest,HttpResponse,HttpInterceptor,HttpEvent } from '@angular/common/http';
import { Observable,of,pipe } from 'rxjs';
import {tap,shareReplay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InterceptService implements HttpInterceptor {
  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    throw new Error('Method not implemented.');
  }
}

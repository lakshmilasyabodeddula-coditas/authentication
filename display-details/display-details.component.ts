import { Component, OnInit,Input } from '@angular/core';
import {ISignUp} from "../types/signUpData.type";

@Component({
  selector: 'app-display-details',
  templateUrl: './display-details.component.html',
  styleUrls: ['./display-details.component.scss']
})
export class DisplayDetailsComponent implements OnInit {
  @Input() userdetails!:ISignUp;
  constructor() { }
  ngOnInit(): void {
  }

}

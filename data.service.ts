import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from "@angular/common/http"
import { ISignUp } from './types/signUpData.type';
import { ILogIn } from './types/logInData.type';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  headers = new HttpHeaders().set("ngrok-skip-browser-warning", "1234");
  constructor(private http:HttpClient) { };

  postSignUpDetails(user:ISignUp)
  {
    return this.http.post(`https://80ee-103-36-44-106.in.ngrok.io/register`,user,{'headers':this.headers})
  }
  postLoginDetails(details:any)
  {
    const ans= this.http.post(`https://80ee-103-36-44-106.in.ngrok.io/login`,details);
    return ans;
  }
  getDetails(accessToken:string,id:number)
  { 
    return this.http.get<ISignUp>("https://80ee-103-36-44-106.in.ngrok.io/user/getUser/"+id,{ headers: {
      "Authorization":"Bearer "+accessToken,
      "ngrok-skip-browser-warning": "1234",
       'Content-type': 'application/json',
      },
      },)
  }
}

import { Component, EventEmitter, OnInit ,Output} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {ISignUp} from "../types/signUpData.type"
// import {DataService} from "../data.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  @Output() signUpEvent=new EventEmitter();
   formGroup!:FormGroup;
  constructor( private formBuilder: FormBuilder) { }
  data!:ISignUp;
  checkoutForm = this.formBuilder.group({
    userName:'',
    userEmail:'',
    password:'',
    userCity:'',
    userrole:'',
    userSalary:''
  });
  onSubmit()
  {
    console.log("sign up form submitted");
    console.log(this.checkoutForm.value.password);
    this.data={
    userName:this.checkoutForm.value.userName,
    userEmail:this.checkoutForm.value.userEmail,
    password:this.checkoutForm.value.password,
    userCity:this.checkoutForm.value.userCity,
    userRole:this.checkoutForm.value.userrole,
    userSalary:this.checkoutForm.value.userSalary
    }
    console.log(this.data);
    this.signUpEvent.emit(this.data);
  }


  ngOnInit(): void {
  }

}

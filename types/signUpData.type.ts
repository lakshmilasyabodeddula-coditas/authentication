export interface ISignUp{
    userName?:string|null
    userEmail?:string|null
    password?:string|null
    userCity?:string|null
    userRole?:string|null
    userSalary?:string|null
}
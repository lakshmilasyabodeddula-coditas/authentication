import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { ILogIn } from '../types/logInData.type';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  @Output() LogInEvent=new EventEmitter();
   formGroup!:FormGroup;
  constructor( private formBuilder: FormBuilder) { }
  data!:ILogIn;
  checkoutForm = this.formBuilder.group({
    userEmail:"",
    password:""
  });
  onSubmit()
  {
    console.log("enterred ")
    this.data={
    userEmail:this.checkoutForm.value.userEmail,
    password:this.checkoutForm.value.password
    }
    console.log(this.data);
    this.LogInEvent.emit(this.data);
  }

  ngOnInit(): void {
  }

}

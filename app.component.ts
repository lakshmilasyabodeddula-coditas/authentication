import { Component, Input } from '@angular/core';
import { DataService} from '../app/data.service';
import {ISignUp} from "../app/types/signUpData.type";
import { ILogIn } from './types/logInData.type';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
   @Input() signupdata:any;
   error!:"";
   stringifieddata="";
   accessToken="";
   userId!:number;

   userDetails !:ISignUp;

   constructor(private dataservice:DataService ){};
   
    onSignUp(data:ISignUp){
      console.log(data);
      this.dataservice.postSignUpDetails(data).subscribe({
        next:(response:any)=>{
          console.log(this.error);
          this.accessToken=response.accessToken;
        },
        error:(error:any)=>{
         this.error=error.message
        },
        complete:()=>{
         this.error="";
        }
      })    
   } 
   onLogIn(details:ILogIn)
   {
      this.dataservice.postLoginDetails(details).subscribe({
        next:(response:any)=>{
          this.error=response;
          this.accessToken=response.jwtToken;
          this.userId=response.jwtId;
        },
        error:(error:any)=>{
         this.error=error.message
        },
        complete:()=>{
         this.error="";
         this.getUserDetails();
        }
      })    
   }
   
   getUserDetails()
   {
    this.dataservice.getDetails(this.accessToken,this.userId).subscribe({
      next:(response:ISignUp)=>{
        console.log(response);
        this.userDetails=response;
        console.log(this.userDetails);
      },
      error:(error:any)=>{
       this.error=error.message
      },
      complete:()=>{
       this.error="";
      }
    })    
 }
}
